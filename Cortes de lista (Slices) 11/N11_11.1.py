locais = ['estados unidos', 'canada', 'franca', 'japao']
print("Os 3 primeiros itens da lista sao: ")
for lugares in locais[:3]:
	print(lugares.title())
print("Os ultimos 3 itens da lista sao: ")
for outros in locais[1:]:
	print(outros.title())
print("Os 2 itens no meio da lista sao: ")
for meio in locais[1:3]:
	print(meio.title())