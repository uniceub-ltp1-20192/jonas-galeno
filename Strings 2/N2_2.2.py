#Variaveis do tipo inteiro
cebolas = 300
cebolas_na_caixa = 120
espaco_caixa = 5
caixas = 60
#Calculo de cebolas fora da caixa, caixas vazias e caixas necessarias
cebolas_fora_da_caixa = (cebolas-cebolas_na_caixa)
caixas_vazias = caixas-(cebolas_na_caixa/espaco_caixa)
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa
#Prints dos resultados
print ("Existem", cebolas_na_caixa, "cebolas encaixotadas")
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print ("Em cada caixa cabem", espaco_caixa, "cebolas")
print ("Ainda temos",caixas_vazias, "caixas vazias")
print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")