#ENTIDADE MAE
class Pessoa:
  
  def _init_(self,identificador = 0, nome="", altura="", peso=""):
    self._identificador = identificador
    self._nome = nome
    self._altura = altura
    self._peso = peso

  @property
  def identificador(self):
    return self._identificador

  @identificador.setter
  def identificador(self,identificador):
    self._identificador = identificador

  @property
  def nome(self):
    return self._nome

  @nome.setter
  def nome(self,nome):
    self._nome = nome

  @property
  def altura(self):
    return self._altura

  @altura.setter
  def altura(self,altura):
    self._altura = altura

  @property
  def peso(self):
    return self._peso

  @peso.setter
  def peso(self,peso):
    self._peso = peso

  def imc(self):
    imc = (self.peso)/(self.altura**2)
    print(imc)

  def fala(self):
    print("Oi meu nome é ", self.nome)

from Entidades.entidadeMae import Pessoa
