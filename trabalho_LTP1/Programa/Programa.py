from Validador.validador import Validador
from Dados.dados import Dados
from Entidades.entidadeFilha import Nacao

#MENU
class Menu:
 
    @staticmethod
    def menuPrincipal():
        print("""
0 - Sair
1 - Consultar
2 - Inserir
3 - Alterar
4 - Deletar
""")
        return Validador.validarOpcaoMenu("[0-4]")
    @staticmethod
    def menuConsultar():
        print("""
0 - Voltar
1 - Consultar por Identificador
2 - Consultar por Propriedade
""")
        return Validador.validarOpcaoMenu("[0-2]")

    @staticmethod
    def iniciarMenu():
        d = Dados()
        opMenu = ""
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()
            if opMenu == "1":
                print("Entrou em Consultar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if(retorno != None):
                            print(retorno)   
                        else:
                            print("""
                            Não foi encontrado 
                            nenhum registro com
                            esse identificador
                            """)                
                        
                    elif opMenu == "2":
                        Menu.menuBuscaPorAtributo(d)
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
               
            elif opMenu == "2":
                print("Entrou em Inserir")
                Menu.menuInserir(d)

            elif opMenu == "3":
                print("Entrou em alterar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                                      

                    elif opMenu == "2":
                        print("Entrou no consultar por Propriedade")
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "4":
                print("Entrou em deletar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno,d)
                    elif opMenu == "2":
                        print("Entrou no consultar por Propriedade")
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "0":
                print("Saindo")
            elif opMenu == "":
                print("")
            else:
                print("Informe uma opção valida")

    @staticmethod
    def menuBuscaPorIdentificador(d :Dados):
        retorno = d.buscarPorIdentificador(Validador.verificarInteiro())
        return retorno

    @staticmethod
    def menuBuscaPorAtributo(d):
        print("Informe o atributo:")
        for index in d :


    def menuInserir(d):
        p = Nacao()
        p.nome = input("Informe o nome:")
        p.altura = input("Informe a altura:")
        p.peso =  input("Informe o peso:")
        p.pais = input("Informe o país:")
        d.inserir(p)

    def menuAlterar(retorno,d):
        retorno.nome = Validador.validarValorInformado(retorno.nome,"Informe o nome:")
        retorno.altura = Validador.validarValorInformado(retorno.altura,"Informe a altura:") 
        retorno.peso = Validador.validarValorInformado(retorno.peso,"Informe o peso:") 
        retorno.pais = Validador.validarValorInformado(retorno.pais,"Informe o país:") 
        d.alterar(retorno)

    def menuDeletar(retorno,d):
        print(retorno)
        deletar = input("""
        Deseja deletar o registro:
        S - Sim
        """)
        if deletar == "S" or deletar == "s":
            d.deletar(retorno)
            print("""
            Registro deletado
            """)
        else:
            print("""
            Registro não foi deletado
            """)
import re

#VALIDADOR
class Validador:

    @staticmethod
    def verificarInteiro():
        teste = False
        while teste == False:
            valor = input("Informe um inteiro:")
            v  = re.match("\d+", valor)
            if v != None:
                teste = True
        return int(valor)

    @staticmethod
    def validarOpcaoMenu(expReg):
        teste = False
        while teste == False:
            opcao = input("Informe uma opção:")
            v  = re.match(expReg,opcao)
            if v != None:
                return opcao
            else:
                print("Opção inválida! Infome um valor entre {} "
                .format(expReg))
     
    @staticmethod
    def validarValorInformado(valorAtual,textoMsg):
        novoValor = input(textoMsg)
        if novoValor != None and novoValor != "":
            return novoValor
        return valorAtual

from Entidades.entidadeFilha import Nacao

#DADOS
class Dados:

    def _init_(self):
        self._dados = dict()
        self.identificador = 0
        
    def buscarPorIdentificador(self,paramIdentificador):
        if (len(self._dados) == 0):
            print("Dicionario vazio!")
        else:
            return self._dados.get(int(paramIdentificador))
   
    def buscarPorAtributo(self,param):
        if (len(self._dados) == 0):
            print("Dicionario vazio!")
        else:
            for x in self._dados.values():
                if x.tamanho == param:
                    return x
            return None

    def inserir(self,entidade):
        entidade.identificador = self.gerarProximoIdentificador()
        self._dados[entidade.identificador] = entidade                  
    
    def alterar(self,entidade):
        self._dados[entidade.identificador] = entidade

#ENTIDADE MAE
class Pessoa:
  
  def _init_(self,identificador = 0, nome="", altura="", peso=""):
    self._identificador = identificador
    self._nome = nome
    self._altura = altura
    self._peso = peso

  @property
  def identificador(self):
    return self._identificador

  @identificador.setter
  def identificador(self,identificador):
    self._identificador = identificador

  @property
  def nome(self):
    return self._nome

  @nome.setter
  def nome(self,nome):
    self._nome = nome

  @property
  def altura(self):
    return self._altura

  @altura.setter
  def altura(self,altura):
    self._altura = altura

  @property
  def peso(self):
    return self._peso

  @peso.setter
  def peso(self,peso):
    self._peso = peso

  def imc(self):
    imc = (self.peso)/(self.altura**2)
    print(imc)

  def fala(self):
    print("Oi meu nome é ", self.nome)

from Entidades.entidadeMae import Pessoa

#ENTIDADE FILHA
class Nacao(Pessoa):
  def _init_(self, pais = "Brasil"):
    super()._init_()
    self._pais = pais

  @property
  def pais(self):
    return self._pais

  @pais.setter
  def pais(self,pais):
    self._pais = pais

  def fala(self):
    print("Oi eu sou do(a) ", self.pais)
  
  def _str_(self):
    return '''
    ---- Pessoa ---- 
    Identificador: {}
    Nome : {}
    Altura : {}
    Peso : {}
    País : {}
    '''.format(self.identificador,self.nome,self.altura,self.peso,self.pais)

from Menu.menu import Menu
from Dados.dados import Dados
from Entidades.entidadeFilha import Nacao

#MAIN
Menu.iniciarMenu()
