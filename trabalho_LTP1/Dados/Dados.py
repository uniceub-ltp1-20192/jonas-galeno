from Entidades.entidadeFilha import Nacao

#DADOS
class Dados:

    def _init_(self):
        self._dados = dict()
        self.identificador = 0
        
    def buscarPorIdentificador(self,paramIdentificador):
        if (len(self._dados) == 0):
            print("Dicionario vazio!")
        else:
            return self._dados.get(int(paramIdentificador))
   
    def buscarPorAtributo(self,param):
        if (len(self._dados) == 0):
            print("Dicionario vazio!")
        else:
            for x in self._dados.values():
                if x.tamanho == param:
                    return x
            return None

    def inserir(self,entidade):
        entidade.identificador = self.gerarProximoIdentificador()
        self._dados[entidade.identificador] = entidade                  
    
    def alterar(self,entidade):
        self._dados[entidade.identificador] = entidade
