#MENU
class Menu:
 
    @staticmethod
    def menuPrincipal():
        print("""
0 - Sair
1 - Consultar
2 - Inserir
3 - Alterar
4 - Deletar
""")
        return Validador.validarOpcaoMenu("[0-4]")
    @staticmethod
    def menuConsultar():
        print("""
0 - Voltar
1 - Consultar por Identificador
2 - Consultar por Propriedade
""")
        return Validador.validarOpcaoMenu("[0-2]")

    @staticmethod
    def iniciarMenu():
        d = Dados()
        opMenu = ""
        while opMenu != "0":
            opMenu = Menu.menuPrincipal()
            if opMenu == "1":
                print("Entrou em Consultar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if(retorno != None):
                            print(retorno)   
                        else:
                            print("""
                            Não foi encontrado 
                            nenhum registro com
                            esse identificador
                            """)                
                        
                    elif opMenu == "2":
                        Menu.menuBuscaPorAtributo(d)
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
               
            elif opMenu == "2":
                print("Entrou em Inserir")
                Menu.menuInserir(d)

            elif opMenu == "3":
                print("Entrou em alterar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuAlterar(retorno,d)
                                      

                    elif opMenu == "2":
                        print("Entrou no consultar por Propriedade")
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "4":
                print("Entrou em deletar")
                while opMenu != "0":
                    opMenu =  Menu.menuConsultar()
                    if opMenu == "1":
                        retorno = Menu.menuBuscaPorIdentificador(d)
                        if retorno != None:
                            Menu.menuDeletar(retorno,d)
                    elif opMenu == "2":
                        print("Entrou no consultar por Propriedade")
                    elif opMenu == "0":
                        print("Voltando")
                opMenu = ""
            elif opMenu == "0":
                print("Saindo")
            elif opMenu == "":
                print("")
            else:
                print("Informe uma opção valida")

    @staticmethod
    def menuBuscaPorIdentificador(d :Dados):
        retorno = d.buscarPorIdentificador(Validador.verificarInteiro())
        return retorno

    @staticmethod
    def menuBuscaPorAtributo(d):
        print("Informe o atributo:")
        for index in d :


    def menuInserir(d):
        p = Nacao()
        p.nome = input("Informe o nome:")
        p.altura = input("Informe a altura:")
        p.peso =  input("Informe o peso:")
        p.pais = input("Informe o país:")
        d.inserir(p)

    def menuAlterar(retorno,d):
        retorno.nome = Validador.validarValorInformado(retorno.nome,"Informe o nome:")
        retorno.altura = Validador.validarValorInformado(retorno.altura,"Informe a altura:") 
        retorno.peso = Validador.validarValorInformado(retorno.peso,"Informe o peso:") 
        retorno.pais = Validador.validarValorInformado(retorno.pais,"Informe o país:") 
        d.alterar(retorno)

    def menuDeletar(retorno,d):
        print(retorno)
        deletar = input("""
        Deseja deletar o registro:
        S - Sim
        """)
        if deletar == "S" or deletar == "s":
            d.deletar(retorno)
            print("""
            Registro deletado
            """)
        else:
            print("""
            Registro não foi deletado
            """)
