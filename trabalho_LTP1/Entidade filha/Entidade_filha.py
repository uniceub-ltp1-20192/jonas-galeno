from Entidades.entidadeMae import Pessoa

#ENTIDADE FILHA
class Nacao(Pessoa):
  def _init_(self, pais = "Brasil"):
    super()._init_()
    self._pais = pais

  @property
  def pais(self):
    return self._pais

  @pais.setter
  def pais(self,pais):
    self._pais = pais

  def fala(self):
    print("Oi eu sou do(a) ", self.pais)
  
  def _str_(self):
    return '''
    ---- Pessoa ---- 
    Identificador: {}
    Nome : {}
    Altura : {}
    Peso : {}
    País : {}
    '''.format(self.identificador,self.nome,self.altura,self.peso,self.pais)